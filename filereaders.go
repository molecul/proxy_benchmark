package main

import (
	"fmt"
	"io/ioutil"
	"strings"
)

func LoadUrls() []string {
	PrintDebugMessage("Reading targets file")
	content, err := ioutil.ReadFile(targets_filename)
	if err != nil {
		PrintErrorMessage("Error on urls loading", err)
	}
	urls := strings.Split(string(content), "\n")
	urls = urls[:len(urls)-1]
	PrintInfoMessage(fmt.Sprintf("Found a %v urls", len(urls)))
	return urls
}

func LoadCreds() []string {
	if credentials_filename != "" {
		PrintInfoMessage("Reading credentials file")
		content, err := ioutil.ReadFile(credentials_filename)
		if err != nil {
			PrintErrorMessage("Error on credentials loading", err)
		}
		users := strings.Split(string(content), "\n")
		users = users[:len(users)-1]
		PrintInfoMessage(fmt.Sprintf("Found a %v credential pairs", len(users)))
		PrintWaringMessage("This tool currently supported only NTLM proxy auth!")
		return users
	} else {
		PrintInfoMessage("Not specified credentials file (requests will be send without proxy authentifications)")
		return nil
	}
}
