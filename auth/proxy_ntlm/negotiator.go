package ntlmssp

import (
	"bytes"
	"encoding/base64"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

func GetDomain(user string) (string, string) {
	domain := ""

	if strings.Contains(user, "\\") {
		ucomponents := strings.SplitN(user, "\\", 2)
		domain = ucomponents[0]
		user = ucomponents[1]
	}
	return user, domain
}

type Negotiator struct{ http.RoundTripper }

func (l Negotiator) RoundTrip(req *http.Request) (res *http.Response, err error) {
	rt := l.RoundTripper
	if rt == nil {
		rt = http.DefaultTransport
	}

	reqauth := authheader(req.Header.Get("X-PerfomanceTool-Auth"))
	req.Header.Del("X-PerfomanceTool-Auth")

	body := bytes.Buffer{}
	if req.Body != nil {
		_, err = body.ReadFrom(req.Body)
		if err != nil {
			log.Println("[Debug]: [Negotiator] Read body.. Exit")
			return nil, err
		}

		req.Body.Close()
		req.Body = ioutil.NopCloser(bytes.NewReader(body.Bytes()))
	}
	res, err = rt.RoundTrip(req)
	if err != nil {
		log.Println("[Debug]: [Negotiator] Try roundtrip.. Exit.. Error: ", err.Error())
		return nil, err
	}
	if res.StatusCode != http.StatusProxyAuthRequired {
		return res, err
	}
	resauth := authheader(res.Header.Get("Proxy-Authenticate"))
	if resauth.IsNTLM() {
		io.Copy(ioutil.Discard, res.Body)
		res.Body.Close()
		res.Header.Get("X-PerfomanceTool-Auth")
		u, p, err := reqauth.GetBasicCreds()
		if err != nil {
			log.Println("[Debug]: [Negotiator] Can't extract creds..")
			return nil, err
		}

		domain := ""
		u, domain = GetDomain(u)

		negotiateMessage, err := NewNegotiateMessage(domain, "")
		if err != nil {
			log.Fatalln(err)
			return nil, err
		}

		if resauth.IsNTLM() {
			req.Header.Set("Proxy-authorization", "NTLM "+base64.StdEncoding.EncodeToString(negotiateMessage))
			req.Header.Set("Proxy-connection", "Keep-Alive")
		}

		req.Body = ioutil.NopCloser(bytes.NewReader(body.Bytes()))

		res, err = rt.RoundTrip(req)
		if err != nil {
			return nil, err
		}

		resauth = authheader(res.Header.Get("Proxy-Authenticate"))
		challengeMessage, err := resauth.GetData()
		if err != nil {
			return nil, err
		}
		if !(resauth.IsNTLM()) || len(challengeMessage) == 0 {
			return res, nil
		}
		io.Copy(ioutil.Discard, res.Body)
		res.Body.Close()

		authenticateMessage, err := ProcessChallenge(challengeMessage, u, p)
		if err != nil {
			return nil, err
		}
		if resauth.IsNTLM() {
			req.Header.Set("Proxy-Authorization", "NTLM "+base64.StdEncoding.EncodeToString(authenticateMessage))
		}

		req.Body = ioutil.NopCloser(bytes.NewReader(body.Bytes()))

		res, err = rt.RoundTrip(req)
	}

	return res, err
}
