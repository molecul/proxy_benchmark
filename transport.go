package main

import (
	"gitlab.com/molecul/proxy_benchmark/auth/proxy_ntlm"

	"crypto/tls"
	"fmt"
	"net"
	"net/http"
	"net/url"
	"time"
)

func PrepareTransferTransport(enableHttps bool) http.Client {
	if enableHttps {
		PrintInfoMessage("Initialize ssl transport")
	}
	var tr *http.Transport
	var keep_alive bool
	var TLSCc *tls.Config

	if enableHttps == true {
		TLSCc = &tls.Config{InsecureSkipVerify: true}
	} else {
		TLSCc = nil
	}

	if proxy_url == "" {
		PrintWaringMessage("All files will be transfer without any proxy!")
		tr = &http.Transport{
			DialContext: (&net.Dialer{
				Timeout:   30 * time.Second,
				KeepAlive: 30 * time.Second,
			}).DialContext,
			MaxIdleConnsPerHost:   10,
			MaxIdleConns:          10,
			IdleConnTimeout:       90 * time.Second,
			TLSHandshakeTimeout:   10 * time.Second,
			ExpectContinueTimeout: 1 * time.Second,
			DisableKeepAlives:     true,
			TLSClientConfig:       TLSCc,
		}

	} else {
		proxyURL, err := url.Parse(proxy_url)
		if err != nil {
			PrintErrorMessage("Can't recognize proxy hostname", err)
		} else {
			PrintInfoMessage(fmt.Sprintf("Use %v proxy", proxy_url))
		}
		keep_alive = true
		if len(Credentials) > 0 {
			PrintWaringMessage("Found a credentials data - should use a Keep-Alives connections")
			keep_alive = false
		}
		tr = &http.Transport{
			Proxy:                 http.ProxyURL(proxyURL),
			MaxIdleConnsPerHost:   3,
			MaxIdleConns:          3,
			IdleConnTimeout:       90 * time.Second,
			TLSHandshakeTimeout:   10 * time.Second,
			ExpectContinueTimeout: 1 * time.Second,
			DisableKeepAlives:     keep_alive,
			TLSClientConfig:       TLSCc,
			DialContext: (&net.Dialer{
				Timeout:   30 * time.Second,
				KeepAlive: 30 * time.Second,
			}).DialContext,
		}
	}

	defer tr.CloseIdleConnections()
	var client http.Client
	if keep_alive {
		client = http.Client{
			Transport: tr,
			Timeout:   180 * time.Second,
		}

	} else {
		client = http.Client{
			Transport: ntlmssp.Negotiator{
				RoundTripper: tr,
			},
			Timeout: 180 * time.Second,
		}
	}

	return client
}
