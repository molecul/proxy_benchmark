package main

import (
	"flag"
	"fmt"
	"net/http"
	"sync/atomic"
	"time"

	"gitlab.com/molecul/proxy_benchmark/utils"
)

var (
	credentials_filename       string
	targets_filename           string
	workers_count              int
	proxy_url                  string
	rounds_count               int
	limitations_urls_per_round int
	debug_enable               bool
)

func ParseProgramArgs() {
	flag.IntVar(&rounds_count, "rounds", 1, "Count of testing rounds")
	flag.IntVar(&workers_count, "threads", 10, "Count of using threads (recommend value 10)")
	flag.IntVar(&limitations_urls_per_round, "max-urls", 0, "Set max urls in one round (for disable limitations set 0 value, if current value more than count of urls in file - limitations was disabled)")
	flag.StringVar(&targets_filename, "target", "list.txt", "Target filename contain urls of collection")
	flag.StringVar(&credentials_filename, "creds", "", "Filename to file with contain users credential for NTLM-proxy-auth")
	flag.StringVar(&proxy_url, "proxy", "", "Proxy url (for example: http://127.0.0.1:3128)")
	flag.BoolVar(&debug_enable, "debug", false, "Enable debug messages")
	flag.Parse()
}

func CalcLimitations(urls []string) uint64 {
	calcTotalRequests := uint64(len(urls) * workers_count * rounds_count)
	PrintInfoMessage(fmt.Sprintf("Using a %v threads", workers_count))
	PrintInfoMessage(fmt.Sprintf("Make a %v testing rounds", rounds_count))
	if limitations_urls_per_round == 0 {
		PrintInfoMessage("No urls limitations per round")
	} else if limitations_urls_per_round < len(urls) {
		PrintInfoMessage(fmt.Sprintf("Max urls per round is %v", limitations_urls_per_round))
		calcTotalRequests = uint64(limitations_urls_per_round * workers_count * rounds_count)
	} else {
		PrintInfoMessage("No urls limitations per round")
		limitations_urls_per_round = 0
	}
	PrintInfoMessage(fmt.Sprintf("Need to send a %v requests", calcTotalRequests))
	return calcTotalRequests
}

func CalcRequestsPerMin() {
	for {
		time.Sleep(time.Minute)
		total_time := time.Now().Unix() - start_time
		opsCurrentl := atomic.LoadUint64(&ops) / 60
		sopsTotal := atomic.LoadUint64(&opsTotal) / uint64(total_time)
		percents := (atomic.LoadUint64(&opsTotal) * 100) / Stats_TotalRequests
		PrintInfoMessage(fmt.Sprintf("Current speed: %v requests per second", opsCurrentl))
		PrintInfoMessage(fmt.Sprintf("Average speed: %v requests per second", sopsTotal))
		PrintInfoMessage(fmt.Sprintf("Average latency: %v ms", atomic.LoadUint64(&total_latency)/atomic.LoadUint64(&opsTotal)))
		PrintInfoMessage(fmt.Sprintf("Requests: %v of %v", atomic.LoadUint64(&opsTotal), Stats_TotalRequests))
		PrintInfoMessage(fmt.Sprintf("Total progress: %v %%", percents))
		atomic.StoreUint64(&ops, 0)
	}
}

func WorkThread(thread_id int, urls []string, client *http.Client, secure_client *http.Client, cut_url_counts int) {

	PrintInfoMessage(fmt.Sprintf("Thread [%v] -> Start urls shuffling", thread_id))

	shuffled_url := utils.ShuffleUrls(urls)
	if cut_url_counts != 0 {
		shuffled_url = shuffled_url[:cut_url_counts]
	}
	PrintInfoMessage(fmt.Sprintf("Thread [%v] -> Urls was shuffled", thread_id))
	for _, url := range shuffled_url {
		var err error
		var status_code int
		var current_body_size uint64

		start_request_time := utils.MakeTimestamp()

		if len(Credentials) > 0 {
			err, current_body_size, status_code = utils.DownloadFile(url, client, secure_client, Credentials)
		} else {
			err, current_body_size, status_code = utils.DownloadFile(url, client, secure_client, nil)
		}

		atomic.AddUint64(&total_latency, uint64(utils.MakeTimestamp()-start_request_time))
		atomic.AddUint64(&ops, 1)
		atomic.AddUint64(&opsTotal, 1)
		atomic.AddUint64(&total_traffic, current_body_size)
		if (err != nil) || (status_code == 403) {
			atomic.AddUint64(&blocked_requests, 1)
		} else if (err != nil) || (status_code != 200) {
			atomic.AddUint64(&error_requests, 1)
			PrintWaringMessage(fmt.Sprintf("Thread [%v] -> Url: %v | Error: %v | status code: %v", thread_id, url, err, status_code))
		}
	}
	wg.Done()
}
