package utils

import (
	"net/http"

	"encoding/base64"
	"io/ioutil"
	"math/rand"
	"strings"
	"time"
)

func GetRandomAuth(users []string) string {
	return users[rand.Intn(len(users))]
}

func DownloadFile(url string, client *http.Client, secure_client *http.Client, auth []string) (error, uint64, int) {
	// Create request
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {

		return err, 0, -1
	}

	req.Header.Set("User-Agent", "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0")
	if len(auth) > 0 {
		req.Header.Set("X-PerfomanceTool-Auth", "NTLM "+base64.StdEncoding.EncodeToString([]byte(GetRandomAuth(auth))))
	}

	// Send responce
	var current_client *http.Client
	if strings.HasPrefix(strings.ToLower(url), "http:") {
		current_client = client
	} else {
		current_client = secure_client
	}

	resp, err := current_client.Do(req)
	if err != nil {
		return err, 0, -1
	}

	// Write the body to ram
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err, 0, resp.StatusCode
	}
	return nil, uint64(len(body)), resp.StatusCode
}

func ShuffleUrls(src []string) []string {
	rand.Seed(time.Now().UTC().UnixNano())
	dest := make([]string, len(src))
	perm := rand.Perm(len(src))
	for i, v := range perm {
		dest[v] = src[i]
	}
	return dest
}

func MakeTimestamp() int64 {
	return time.Now().UnixNano() / int64(time.Millisecond)
}
