package main

import (
	"log"
)

func PrintInfoMessage(msg string) {
	log.Printf("[Info]: %v\n", msg)
}

func PrintDebugMessage(msg string) {
	if debug_enable {
		log.Printf("[Debug]: %v\n", msg)
	}
}

func PrintWaringMessage(msg string) {
	log.Printf("[Warn]: %v\n", msg)
}

func PrintErrorMessage(msg string, err error) {
	log.Fatalf("[Error]: %v: %v\n", msg, err)
}
