package main

import (
	"fmt"
	"log"
	"math/rand"
	"sync"
	"sync/atomic"
	"time"

	"gitlab.com/molecul/proxy_benchmark/utils"
)

var (
	Stats_TotalRequests uint64
	Credentials         []string

	wg               sync.WaitGroup
	ops              uint64
	opsTotal         uint64
	start_time       int64
	error_requests   uint64
	blocked_requests uint64
	total_traffic    uint64
	total_latency    uint64
)

func init() {
	rand.Seed(time.Now().Unix())
	utils.ShowBanner()
	PrintInfoMessage("Program was started")
	ParseProgramArgs()

}

func main() {
	total_urls := LoadUrls()
	Credentials = LoadCreds()

	Stats_TotalRequests = CalcLimitations(total_urls)

	httpClient := PrepareTransferTransport(false)
	httpsClient := PrepareTransferTransport(true)

	start_time = time.Now().Unix()

	go CalcRequestsPerMin()
	for j := 1; j <= rounds_count; j++ {
		wg.Add(workers_count)
		PrintInfoMessage(fmt.Sprintf("Run a %v round from %v.\n", j, rounds_count))
		for i := 0; i < workers_count; i++ {
			go WorkThread(i, total_urls, &httpClient, &httpsClient, limitations_urls_per_round)
		}
		wg.Wait()
	}
	wg.Wait()
	total_time := time.Now().Unix() - start_time
	SopsTotal := atomic.LoadUint64(&opsTotal) / uint64(total_time)
	log.Println("[Info]: Benchmark was finished!")
	log.Printf("[Info]: Average speed: %v requests per second.\n", SopsTotal)
	log.Printf("[Info]: Total spend time: %v seconds.\n", total_time)
	log.Printf("[Info]: Total requests: %v.\n", atomic.LoadUint64(&opsTotal))
	log.Printf("[Info]: Total rounds: %v.\n", rounds_count)
	log.Printf("[Info]: Total traffic: %v bytes.\n", atomic.LoadUint64(&total_traffic))
	log.Printf("[Info]: Total average latency: %v ms.\n", atomic.LoadUint64(&total_latency)/atomic.LoadUint64(&opsTotal))
	log.Printf("[Info]: Blocked requests: %v.\n", atomic.LoadUint64(&blocked_requests))
	log.Printf("[Info]: Transfer errors: %v.\n", atomic.LoadUint64(&error_requests))
}
